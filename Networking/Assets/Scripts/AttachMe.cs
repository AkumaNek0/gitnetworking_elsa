using UnityEngine;

public class AttachMe : MonoBehaviour // Script d'Ivan pour rester accroch� � une platform.
{
    public LayerMask layerFilter;

    bool InLayer(LayerMask objLayer)
    {
        return (layerFilter.value & 1 << objLayer) > 0;
    }

    void OnCollisionEnter2D(Collision2D other) // A l'entr�e parente moi !
    {
        if (InLayer(other.gameObject.layer))
        {
            if (other.rigidbody)
                other.rigidbody.transform.parent = transform;
            else
                other.transform.parent = transform;
        }
    }

    void OnCollisionExit2D(Collision2D other) // A la sortie d�parente moi !
    {
        if (InLayer(other.gameObject.layer))
        {
            if (other.rigidbody)
                other.rigidbody.transform.parent = null;
            else
                other.transform.parent = null;
        }
    }
}
