using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Enemy : MonoBehaviour
{
    [SerializeField] Transform point1, point2;
    public Vector3 position1;
    public Vector3 position2;
    private PhotonView PV;
    public float EnemySpeed = 1;
    public SpriteRenderer spriteMush;

    Vector3 currentTargetDestination;

    public float distanceTolerance = 0.5f; //you can change the tolerance to whatever you need it to be

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    void Start()
    {
        if (PhotonNetwork.IsMasterClient == false)
            return;
        position1 = point1.position;
        position2 = point2.position;
        transform.position = position1; //set the initial position
        currentTargetDestination = position2;
    }

    void Update()
    {
        if (PhotonNetwork.IsMasterClient == false)
            return;
        transform.position = Vector3.MoveTowards(transform.position, currentTargetDestination, EnemySpeed * Time.deltaTime);

        if (Vector3.Distance(transform.position, currentTargetDestination) <= distanceTolerance)
        {
            //once we reach the current destination, set the other location as our new destination
            if (currentTargetDestination == position1)
            {
                currentTargetDestination = position2;
            }
            else
            {
                currentTargetDestination = position1;
            }
        }
    }
}
