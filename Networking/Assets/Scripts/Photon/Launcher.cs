using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Launcher : MonoBehaviourPunCallbacks
{
    public static Launcher Instance;
    [SerializeField] Transform RoomImage;
    [SerializeField] GameObject ButtonRoomPrefab;
    [SerializeField] TMP_InputField roomNameInputField;
    [SerializeField] TMP_Text roomNameText;
    [SerializeField] Transform PseudoImage;
    [SerializeField] GameObject PseudoPrefab;
    [SerializeField] Button StartGameButton; 
    [SerializeField] Button CreateButtonMainMenu;
    [SerializeField] Button JoinButtonMainMenu;
    [SerializeField] TMP_InputField InputFieldMainMenu;
    

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if(PhotonNetwork.IsMasterClient)
        {
            StartGameButton.interactable = true;
        } 
    }
    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("Joined lobby :3");
        MenuManager.Instance.openMenu("MainMenu");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer) //si player join room, on le recoit
    {
        GameObject myNewPseudo = Instantiate(PseudoPrefab, PseudoImage);
        myNewPseudo.GetComponent<roomPlayerListItem>().Setup(newPlayer);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer) //same mais il part
    {
        Player[] players = PhotonNetwork.PlayerList; //recupere tt les players de la liste

        foreach(Transform transform in PseudoImage) //detruit tous les prefabs
        {
            Destroy(transform.gameObject);
        }

        for(int i = 0; i < players.Length; i++)
        {
            GameObject myNewPseudo = Instantiate(PseudoPrefab, PseudoImage);
            myNewPseudo.GetComponent<roomPlayerListItem>().Setup(players[i]);
        }
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach(Transform transform in RoomImage)
        {
            Destroy(transform.gameObject);
        }
        for(int i = 0; i < roomList.Count; i++)
        {
            if(roomList[i].RemovedFromList == false)
                Instantiate(ButtonRoomPrefab, RoomImage).GetComponent<RoomItemButton>().SetupRoomButton(roomList[i]);
        }
    }
    public void createRoom()
    {
        if(string.IsNullOrEmpty(roomNameInputField.text))
        {
            return;
        }
        MenuManager.Instance.openMenu("LoadingMenu");
        PhotonNetwork.CreateRoom(roomNameInputField.text);
    }

    public void LaunchGame()
    {
        PhotonNetwork.LoadLevel(1);
    }
    public override void OnJoinedRoom()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            StartGameButton.interactable = true;
        }

        roomNameText.text = PhotonNetwork.CurrentRoom.Name;

        Player[] players = PhotonNetwork.PlayerList; //recupere tt les players de la liste

        foreach(Transform transform in PseudoImage) //detruit tous les prefabs
        {
            Destroy(transform.gameObject);
        }

        for(int i = 0; i < players.Length; i++)
        {
            GameObject myNewPseudo = Instantiate(PseudoPrefab, PseudoImage);
            myNewPseudo.GetComponent<roomPlayerListItem>().Setup(players[i]);
        }

        MenuManager.Instance.openMenu("RoomMenu");
    }

    public void setUsername(string actualUsername)
    {
        if (InputFieldMainMenu.text == null || InputFieldMainMenu.text == "")
        {
            CreateButtonMainMenu.interactable = false;
            JoinButtonMainMenu.interactable = false;
            return;
        }
        else
        {
            CreateButtonMainMenu.interactable = true;
            JoinButtonMainMenu.interactable = true;
        }
        
        PhotonNetwork.NickName = actualUsername;
    }
}
