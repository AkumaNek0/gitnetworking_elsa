﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MoveControllerIvan : MonoBehaviour
{
    [Header("General:")]
    public float moveSpeed = 2.5f;
    public Transform toRotateWithX;
    private Animator animator;
    private PhotonView PV;
    private SpriteRenderer spriteRenderer;
    public PlayerManager manager;
    private bool Move = true;


    [Header("PhysX specifics:")]
    public Collider2D myFeet;
    public Rigidbody2D _playerRigidBody;
    public Camera cameraPlayer;
    public float jumpForce = 5.5f;
    

    void Awake()
    {
        animator = GetComponent<Animator>();
        PV = GetComponent<PhotonView>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        if(PV.IsMine == false)
        {
            Destroy(_playerRigidBody);
            Destroy(cameraPlayer);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }

        if(PV.IsMine == false)
            return;
        
        MovePlayer();

        if(myFeet.IsTouchingLayers())
        {
            animator.SetBool("OnGround", true);
        }
        else{animator.SetBool("OnGround", false);}

        //flip sprite
        if(Input.GetAxisRaw("Horizontal") > 0)
        {
            spriteRenderer.flipX = false;
        }
        else if(Input.GetAxisRaw("Horizontal") < 0)
        {
            spriteRenderer.flipX = true;
        }
    }

    public void Jump()
    {
        if (_playerRigidBody && myFeet && myFeet.IsTouchingLayers())
            {
                _playerRigidBody.velocity = new Vector2(_playerRigidBody.velocity.x, 0);
                _playerRigidBody.velocity += Vector2.up * jumpForce;
                animator.SetTrigger("Jump");
                animator.SetBool("OnGround", false);
            }
    }

    public void Flip(float direction)
    {
        if (!toRotateWithX)
            return;

        if (direction > 0)
            toRotateWithX.rotation = Quaternion.Euler(0, 0, 0);
        else if (direction < 0)
            toRotateWithX.rotation = Quaternion.Euler(0, -180, 0);
    }

    private void MovePlayer()
    {
        if(Move == true)
        {
            var horizontalInput = Input.GetAxisRaw("Horizontal");
        _playerRigidBody.velocity = new Vector2(horizontalInput * moveSpeed, _playerRigidBody.velocity.y);
        
        if (_playerRigidBody.velocity.x < 0 || _playerRigidBody.velocity.x > 0)
        {
            animator.SetBool("Run", true);
        }
        else
        {
            animator.SetBool("Run", false);
        }
        }
        else
        {
            return;
        }
        

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            if(PV.IsMine == true)
                StartCoroutine("Die");
        }
    }
    public IEnumerator Die()
    {
        //if(PV.IsMine == true)
        //{
            animator.SetTrigger("Hit");
            _playerRigidBody.Sleep();
            Move = false;
            yield return new WaitForSeconds(1);

            gameObject.transform.position = manager.Spawnpoint.position;
            animator.SetTrigger("Respawn");
            _playerRigidBody.WakeUp();
            Move = true;
        //}
        //else {}
    }
}
