using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Photon.Pun;

public class PlayerManager : MonoBehaviour
{
    public PhotonView PV;
    public GameObject Controller;
    private Animator animator;
    public Enemy enemyPrefab;
    public Transform Spawnpoint;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    private void Start()
    {
        if(PV.IsMine)
            createController();
    }

    public void createController()
    {
        Controller = PhotonNetwork.Instantiate(Path.Combine("Prefabs", "PlayerController"), Spawnpoint.position, Quaternion.identity);
        animator = GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            StartCoroutine("Die");
        }
    }

    
}
